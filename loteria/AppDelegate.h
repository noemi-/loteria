//
//  AppDelegate.h
//  loteria
//
//  Created by Noemi Quezada on 6/4/15.
//  Copyright (c) 2015 Noemi Quezada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

